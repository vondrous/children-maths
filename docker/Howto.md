# Build Image
`docker build --pull --rm -f "children-math\dockerfile" -t children-math:latest "children-math"`

# Run Image
`docker run --rm -it  children-math:latest`

