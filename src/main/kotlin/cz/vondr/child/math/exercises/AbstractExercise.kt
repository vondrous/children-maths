package cz.vondr.child.math.exercises

import cz.vondr.child.math.ExerciseContext

abstract class AbstractExercise(val context: ExerciseContext) {

    abstract fun getResult(): Int
    abstract fun getFormattedExercise(): String
    lateinit var answer: String

    private var incorrectAnswerCounted = false

    fun execute() {
        var parsedSuccessfully = false
        while (!parsedSuccessfully) {
            try {
                println(getFormattedExercise())
                answer = readLine()!!
                println("Zadal jsi '$answer'")
                val answerInt = answer.trim().toInt()
                parsedSuccessfully = true
                evaluate(answerInt)
            } catch (e: NumberFormatException) {
                println("'$answer' nelze načíst. Zadej číslo.")
                println()
            }
        }

    }

    protected fun evaluate(answerInt: Int) {
        if (answerInt == getResult()) {
            if (!incorrectAnswerCounted) {
                context.addCorrectAnswer()
            }
            println("Správně :-) :-) :-) :-) :-) :-) :-) :-) :-) :-) :-)")
        } else {
            if (!incorrectAnswerCounted) {
                context.addIncorrectAnswer()
                incorrectAnswerCounted = true
            }
            wrongAnswer()
        }
    }

    private fun wrongAnswer() {
        val againOnWrongAnswer = true
        if (againOnWrongAnswer) {
            println("NEEE CHYBA !!! Zkus to znovu. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            execute()
        } else {
            println("NEEE - zadal jsi '$answer', ale melo to byt '${getResult()}'!!!!!!!!!!!!!!!")
        }
    }
}