package cz.vondr.child.math.exercises

import cz.vondr.child.math.ExerciseContext
import java.lang.Math.min
import kotlin.random.Random

class SimpleAddition(context: ExerciseContext, minResult: Int = 0, maxResult: Int = 10, minNumber: Int = 0) :
    AbstractExercise(context) {
    init {
        if (minResult + 1 <= 2 * minNumber) {
            throw IllegalArgumentException(
                "Invalid input values = minResult=$minResult, maxResult=$maxResult, minNumber=$minNumber\n" +
                        "This condition must be sutisfied: 'minResult + 1 > 2*minNumber'"
            )
        }
    }

    private val result = Random.nextInt(minResult, maxResult + 1)
    val b = Random.nextInt(minNumber, result + 1 - minNumber)
    val a = result - b

    override fun getResult(): Int {
        return result
    }

    override fun getFormattedExercise(): String {
        return "$a + $b ="
    }

}

class AdditionBelowEachOther(context: ExerciseContext, minResult: Int = 0, maxResult: Int = 1000, minNumber: Int = 0) :
    AbstractExercise(context) {

    private val result = Random.nextInt(minResult, maxResult + 1)
    val b = Random.nextInt(minNumber, result + 1 - minNumber)
    val a = result - b

    override fun getResult(): Int {
        return result
    }

    override fun getFormattedExercise(): String {
        val pad = listOf(a, b).maxOf { it.toString().length }
        return """ 
            |  ${a.toString().padStart(pad)}
            |+ ${b.toString().padStart(pad)}
            |=""".trimMargin()
    }

}

class TensAddition(context: ExerciseContext) : AbstractExercise(context) {

    private val result = Random.nextInt(11)
    val b = Random.nextInt(result + 1)
    val a = result - b

    override fun getResult(): Int {
        return result * 10
    }

    override fun getFormattedExercise(): String {
        return "${a * 10} + ${b * 10} ="
    }

}

class AboveTenAddition(context: ExerciseContext) : AbstractExercise(context) {

    private var result = Random.nextInt(11)
    var b = Random.nextInt(result + 1)
    var a = result - b

    init {
        if (Random.nextBoolean()) {
            a += 10
        } else {
            b += 10
        }
        result += 10
    }

    override fun getResult(): Int {
        return result
    }

    override fun getFormattedExercise(): String {
        return "${a} + ${b} ="
    }

}

class SimpleSubtraction(context: ExerciseContext, minNumber: Int = 0, maxNumber: Int = 10, minSecondNumber: Int = 0) :
    AbstractExercise(context) {

    val a = Random.nextInt(minNumber, maxNumber + 1)
    val b = Random.nextInt(minSecondNumber, a + 1 - minSecondNumber)
    private val result = a - b

    override fun getResult(): Int {
        return result
    }

    override fun getFormattedExercise(): String {
        return "$a - $b ="
    }

}

class SubtractionBelowEachOther(
    context: ExerciseContext,
    minNumber: Int = 0,
    maxNumber: Int = 1000,
    minSecondNumber: Int = 0
) :
    AbstractExercise(context) {

    val a = Random.nextInt(minNumber, maxNumber + 1)
    val b = Random.nextInt(minSecondNumber, a + 1 - minSecondNumber)
    private val result = a - b

    override fun getResult(): Int {
        return result
    }

    override fun getFormattedExercise(): String {
        val pad = listOf(a, b).maxOf { it.toString().length }
        return """ 
            |  ${a.toString().padStart(pad)}
            |- ${b.toString().padStart(pad)}
            |=""".trimMargin()
    }

}

class TensSubtraction(context: ExerciseContext) : AbstractExercise(context) {

    val a = Random.nextInt(11)
    val b = Random.nextInt(a + 1)
    private val result = a - b

    override fun getResult(): Int {
        return result * 10
    }

    override fun getFormattedExercise(): String {
        return "${a * 10} - ${b * 10} ="
    }

}

class AboveTenSubtraction(context: ExerciseContext) : AbstractExercise(context) {

    var a = Random.nextInt(11)
    var b = Random.nextInt(a + 1)
    private var result = a - b

    init {
        a += 10
        result += 10
    }

    override fun getResult(): Int {
        return result
    }

    override fun getFormattedExercise(): String {
        return "${a} - ${b} ="
    }

}

class Multiplication(
    context: ExerciseContext,
    first: IntRange = 1..10,
    second: IntRange = 1..10,
    swap: Boolean = false
) :
    AbstractExercise(context) {

    var a = Random.nextInt(first.first, first.last + 1)
    var b = Random.nextInt(second.first, second.last + 1)
    private var result = a * b

    init {
        if (swap && Random.nextBoolean()) {
            a = b.also { b = a }
        }
    }

    override fun getResult(): Int {
        return result
    }

    override fun getFormattedExercise(): String {
        return "${a} * ${b} ="
    }

}

class AdditionRanges(
    context: ExerciseContext,
    first: IntRange = 1..10,
    second: IntRange = 1..10,
    swap: Boolean = false
) :
    AbstractExercise(context) {

    var a = Random.nextInt(first.first, first.last + 1)
    var b = Random.nextInt(second.first, second.last + 1)
    private var result = a + b

    init {
        if (swap && Random.nextBoolean()) {
            a = b.also { b = a }
        }
    }

    override fun getResult(): Int {
        return result
    }

    override fun getFormattedExercise(): String {
        return "${a} + ${b} ="
    }

}

/**
 * @param minResult Result will never be less than specified value
 *
 * Note some restrictions can throw exception
 */
class SubtractionRanges(
    context: ExerciseContext,
    first: IntRange = 10..10,
    second: IntRange = 1..10,
    minResult: Int = -1_000_000_000
) :
    AbstractExercise(context) {

    var a = Random.nextInt(first.first, first.last + 1)
    var b = Random.nextInt(second.first, min(a - minResult, second.last) + 1)

    private var result = a - b

    override fun getResult(): Int {
        return result
    }

    override fun getFormattedExercise(): String {
        return "${a} - ${b} ="
    }

}

class Division(
    context: ExerciseContext,
    result: IntRange = 1..10,
    second: IntRange = 1..10,
) :
    AbstractExercise(context) {

    var r = Random.nextInt(result.first, result.last + 1)
    var b = Random.nextInt(second.first, second.last + 1)
    private var a = b * r

    override fun getResult(): Int {
        return r
    }

    override fun getFormattedExercise(): String {
        return "${a} / ${b} ="
    }

}

class SubtractionDefinedByResult(
    context: ExerciseContext,
    result: IntRange = 1..10,
    second: IntRange = 1..10,
) :
    AbstractExercise(context) {

    var r = Random.nextInt(result.first, result.last + 1)
    var b = Random.nextInt(second.first, second.last + 1)
    private var a = b + r

    override fun getResult(): Int {
        return r
    }

    override fun getFormattedExercise(): String {
        return "${a} - ${b} ="
    }
}