package cz.vondr.child.math.config

import cz.vondr.child.math.Context
import cz.vondr.child.math.exercises.*


class Config(
    val username: String = "DefaultUser",
    val exerciseCount: Int,
    val exercises: List<ExerciseFactory>
) {
    companion object {
        val default = Config(
            exerciseCount = 3,
            exercises = listOf(
                Probability(1) of { ctx -> SimpleAddition(ctx, minResult = 0, maxResult = 10, minNumber = 0) },
                Probability(1) of { ctx -> SimpleSubtraction(ctx, minNumber = 0, maxNumber = 10, minSecondNumber = 0) },
                Probability(1) of { ctx -> TensAddition(ctx) },
                Probability(1) of { ctx -> TensSubtraction(ctx) },
                Probability(1) of { ctx -> AboveTenAddition(ctx) },
                Probability(1) of { ctx -> AboveTenSubtraction(ctx) },
                Probability(1) of { ctx -> Multiplication(ctx, first = 1..10, second = 1..10, swap = true) },
                Probability(1) of { ctx -> AdditionRanges(ctx, first = 1..10, second = 1..10, swap = true) },
                Probability(1) of { ctx -> SubtractionRanges(ctx, first = 10..20, second = 1..10) },
                Probability(1) of { ctx -> Division(ctx, result = 1..10, second = 1..10) },
                Probability(1) of { ctx -> AdditionBelowEachOther(ctx, minResult = 0, maxResult = 1000, minNumber = 0) },
                Probability(1) of { ctx -> SubtractionBelowEachOther(ctx, minNumber = 0, maxNumber = 1000, minSecondNumber = 0) },
                Probability(1) of { ctx -> SubtractionDefinedByResult(ctx, result = 8..9, second = 2..10) },
            )
        )
    }

}

@Suppress("EXPERIMENTAL_FEATURE_WARNING")
inline class Probability(val value: Int)


data class ExerciseFactory(val probability: Probability, val factory: (Context) -> AbstractExercise)

infix fun Probability.of(that: (Context) -> AbstractExercise): ExerciseFactory =
    ExerciseFactory(this, that)
