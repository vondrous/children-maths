package cz.vondr.child.math

import cz.vondr.child.math.config.Config
import cz.vondr.child.math.exercises.AbstractExercise
import org.apache.commons.lang3.time.StopWatch
import org.slf4j.LoggerFactory
import kotlin.random.Random

class Math(val config: Config = Config.default) {

    private val logger = LoggerFactory.getLogger(javaClass)

    fun run() {
        try {
            internalRun()
        } catch (e: Exception) {
            logger.error("Fatal error occurred.", e)
            throw e
        }
    }

    private fun internalRun() {
        val ctx = Context(config)
        (1..ctx.config.exerciseCount).forEach { exerciseNumber ->
            println("zbývá " + "${ctx.config.exerciseCount + 1 - exerciseNumber}")
            runOneExercise(ctx)
            println()
        }
        writeResults(ctx)
        readEnd()
    }

    private fun writeResults(ctx: Context) {
        val countsMessage =
            "HOTOVO - příkladů: ${ctx.config.exerciseCount} , správně: ${ctx.correctCount}, špatně: ${ctx.inCorrectCount}"
        val timeMesage = "Čas: ${ctx.stopWatch}"
        println(countsMessage)
        println(timeMesage)
        logger.info("User: '${ctx.config.username}' $countsMessage $timeMesage")
    }

    private fun runOneExercise(context: Context) {
        val exercise = generateNextExercise(context)
        exercise.execute()
    }

    private fun generateNextExercise(context: Context): AbstractExercise {
        val exercises = context.config.exercises
        val probabilitySum = exercises.map { it.probability.value }.sum()
        val random = Random.nextInt(0, probabilitySum)
        var actual = 0
        exercises.forEach {
            actual += it.probability.value
            if (actual > random) {
                return it.factory(context)
            }
        }
        throw IllegalStateException(config.toString())
    }

    private fun readEnd() {
        val possibleEndWords = listOf("end", "konec", "e", "exit", "quit", "q", "k")
        var input = ""
        while (!possibleEndWords.any { it.equals(input, true) }) {
            println("Napiš 'End' pro ukončení.")
            input = readLine()!!.trim()
        }
    }

}

interface ExerciseContext {
    fun addCorrectAnswer()
    fun addIncorrectAnswer()
}

class Context(val config: Config) : ExerciseContext {
    var correctCount = 0
    var inCorrectCount = 0

    var stopWatch = StopWatch.createStarted()

    override fun addCorrectAnswer() {
        correctCount++
    }

    override fun addIncorrectAnswer() {
        inCorrectCount++
    }

}




