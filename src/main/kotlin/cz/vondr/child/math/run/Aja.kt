package cz.vondr.child.math.run

import cz.vondr.child.math.Math
import cz.vondr.child.math.config.Config
import cz.vondr.child.math.config.Probability
import cz.vondr.child.math.config.of
import cz.vondr.child.math.exercises.*

fun main() {

    val config = Config(
        username = "Ája",
        exerciseCount = 10,
        exercises = listOf(
//            Probability(0) of { ctx -> SimpleAddition(ctx, minResult = 0, maxResult = 100, minNumber = 0) },
//            Probability(0) of { ctx -> SimpleSubtraction(ctx, minNumber = 0, maxNumber = 100, minSecondNumber = 0) },
//            Probability(0) of { ctx -> Multiplication(ctx, first = 3..5, second = 2..10, swap = true) },
//            Probability(4) of { ctx -> Multiplication(ctx, first = 6..9, second = 6..9, swap = true) },
//            Probability(4) of { ctx -> Division(ctx, result = 1..10, second = 6..9) },
            Probability(1) of { ctx -> AdditionBelowEachOther(ctx,minResult = 0, maxResult = 1000, minNumber = 0) },
            Probability(1) of { ctx -> SubtractionBelowEachOther(ctx, minNumber = 0, maxNumber = 1000, minSecondNumber = 0) },
            Probability(1) of { ctx -> Multiplication(ctx, first = 1..1000, second = 2..9, swap = false) },
        )
    )

    Math(config).run()
}