package cz.vondr.child.math.run

import cz.vondr.child.math.Math
import cz.vondr.child.math.config.Config
import cz.vondr.child.math.config.Probability
import cz.vondr.child.math.config.of
import cz.vondr.child.math.exercises.*

fun main() {

    val config = Config(
        username = "Majda",
        exerciseCount = 20,


        exercises = listOf(
//            Probability(10) of { ctx -> AdditionRanges(ctx, first = 0..90, second = 2..10, swap = true) },
//            Probability(10) of { ctx -> SubtractionRanges(ctx, first = 10..100, second = 2..10) },

            Probability(10) of { ctx -> SimpleAddition(ctx, minResult = 5, maxResult = 1000, minNumber = 2) },

//            Probability(10) of { ctx -> SimpleAddition(ctx, minResult = 5, maxResult = 100, minNumber = 2) },
            Probability(10) of { ctx -> SimpleSubtraction(ctx, minNumber = 5, maxNumber = 1000, minSecondNumber = 2) },

            Probability(10) of { ctx -> Multiplication(ctx, first = 1..100, second = 1..10, swap = true) },
            Probability(10) of { ctx -> Division(ctx, result = 1..10, second = 1..10) },

            )
    )

    Math(config).run()
}
