package cz.vondr.child.math.run

import cz.vondr.child.math.Math
import cz.vondr.child.math.config.Config
import cz.vondr.child.math.config.Probability
import cz.vondr.child.math.config.of
import cz.vondr.child.math.exercises.*

fun main() {

    val config = Config(
        username = "Majda6a7",
        exerciseCount = 20,


        exercises = listOf(
            Probability(10) of { ctx -> Multiplication(ctx, first = 6..9, second = 6..9, swap = true) },
            Probability(10) of { ctx -> Division(ctx, result = 6..9, second = 6..9) },
        )
    )

    Math(config).run()
}
