package cz.vondr.child.math.run

import cz.vondr.child.math.Math
import cz.vondr.child.math.config.Config
import cz.vondr.child.math.config.Probability
import cz.vondr.child.math.config.of
import cz.vondr.child.math.exercises.Division
import cz.vondr.child.math.exercises.Multiplication

fun main() {

    val config = Config(
        username = "Jeník",
        exerciseCount = 20,

        exercises = listOf(
//            Probability(2) of { ctx -> SimpleAddition(ctx, minResult = 2, maxResult = 10, minNumber = 1) },
//            Probability(2) of { ctx -> SimpleSubtraction(ctx, minNumber = 2, maxNumber = 10, minSecondNumber = 1) },
//            Probability(2) of { ctx -> AboveTenAddition(ctx) },
//            Probability(2) of { ctx -> AboveTenSubtraction(ctx) },
//            Probability(10) of { ctx -> AdditionRanges(ctx, first = 2..9, second = 2..9, swap = true) },
//            Probability(10) of { ctx -> SubtractionDefinedByResult(ctx, result = 2..9, second = 2..9) },

            // Addition and subtraction up to 100
//            Probability(10) of { ctx -> SimpleAddition(ctx, minResult = 2, maxResult = 100, minNumber = 1) },
//            Probability(10) of { ctx -> SimpleSubtraction(ctx, minNumber = 2, maxNumber = 100, minSecondNumber = 1) },

            // Multiplication and division
//            Probability(5) of { ctx -> Multiplication(ctx, first = 1..10, second = 2..2, swap = true) },
//            Probability(5) of { ctx -> Division(ctx, result = 1..10, 2..2) },
//
//            Probability(10) of { ctx -> Multiplication(ctx, first = 1..10, second = 3..3, swap = true) },
//            Probability(10) of { ctx -> Division(ctx, result = 1..10, 3..3) },

//            Probability(5) of { ctx -> Multiplication(ctx, first = 1..10, second = 2..2, swap = true) },
//            Probability(10) of { ctx -> Multiplication(ctx, first = 1..10, second = 3..3, swap = true) },

            Probability(10) of { ctx -> Multiplication(ctx, first = 1..9, second = 1..9, swap = true) },
            Probability(10) of { ctx -> Division(ctx, result = 1..9, 1..9) },

            )
    )

    Math(config).run()
}
