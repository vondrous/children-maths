import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.gradle.api.tasks.testing.logging.TestLogEvent.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "cz.vondr"
version = "1.0-SNAPSHOT"

// KOTLIN SETUP -------------------------------------
plugins {
    kotlin("jvm") version "1.4.10"
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.apache.commons:commons-lang3:3.9")
}

repositories {
    mavenCentral()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

// jUnit5 - SETUP ----------------------------------
tasks.withType<Test> {
    useJUnitPlatform()
}

val junitVersion = "5.4.2"
val log4jVersion = "2.17.1"
dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-params:$junitVersion")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:$junitVersion")

    implementation("org.apache.logging.log4j:log4j-core:$log4jVersion")
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:$log4jVersion")
    implementation("org.apache.logging.log4j:log4j-1.2-api:$log4jVersion")
}

tasks.withType<Test> {
    testLogging {
        exceptionFormat = FULL
        events = setOf(PASSED, STARTED, FAILED, SKIPPED)
    }
}

// PROJECT SPECIFIC SETUP -------------------------

dependencies {
//    implementation("org.apache.commons:commons-lang3:3.9")

    testImplementation("org.assertj:assertj-core:2.9.1")
}

tasks {

    val run by registering(JavaExec::class) {
        group = "!!!tasks!!!"

        main = "cz.vondr.child.math.run.MathKt"
        classpath = sourceSets["main"].runtimeClasspath

        standardInput = System.`in`
        //systemProperties = mapOf("file.encoding" to "utf-8")
        systemProperties = mapOf("file.encoding" to "cp852") // to detect/set  encoding use system command "chcp"

        // To start with debugger => set 'true'
        debug = false
    }

    val runAja by registering(JavaExec::class) {
        group = "!!!tasks!!!"

        main = "cz.vondr.child.math.run.AjaKt"
        classpath = sourceSets["main"].runtimeClasspath

        standardInput = System.`in`
        //systemProperties = mapOf("file.encoding" to "utf-8")
        systemProperties = mapOf("file.encoding" to "cp852") // to detect/set  encoding use system command "chcp"
    }

    val runMajda by registering(JavaExec::class) {
        group = "!!!tasks!!!"

        main = "cz.vondr.child.math.run.MajdaKt"
        classpath = sourceSets["main"].runtimeClasspath

        standardInput = System.`in`
        //systemProperties = mapOf("file.encoding" to "utf-8")
        systemProperties = mapOf("file.encoding" to "cp852") // to detect/set  encoding use system command "chcp"
    }

    val runJenik by registering(JavaExec::class) {
        group = "!!!tasks!!!"

        main = "cz.vondr.child.math.run.JenikKt"
        classpath = sourceSets["main"].runtimeClasspath

        standardInput = System.`in`
//        systemProperties = mapOf("file.encoding" to "utf-8")
        systemProperties = mapOf("file.encoding" to "cp852") // to detect/set  encoding use system command "chcp"
    }

    val runUniversal by registering(JavaExec::class) {
        group = "!!!tasks!!!"

        val mainClassPropertyName = "main-class"
        main = if (project.hasProperty(mainClassPropertyName)) {
            project.property(mainClassPropertyName) as String
        } else {
            "cz.vondr.child.math.run.ErrorKt"
        }

        classpath = sourceSets["main"].runtimeClasspath

        standardInput = System.`in`
        //systemProperties = mapOf("file.encoding" to "utf-8")
        systemProperties = mapOf("file.encoding" to "cp852") // to detect/set  encoding use system command "chcp"
    }

}



